﻿$(document).ready(function () {
    $("#dateOfBirth").datepicker({ dateFormat: 'dd/mm/yy', maxDate: -1 });
    $("#submit").prop('disabled', true);

    $("#calculatorForm").on("submit", function (e) {
        submitCalculatorForm();
        e.preventDefault();
    });

    $("#dateOfBirth").on("keyup", function (e) {
        ValidateDate();
    });
    $("#dateOfBirth").on("change", function (e) { //.change(function(){
        ValidateDate();
    });
    $("#dateError").hide();
    $("#age").hide();
});

function submitCalculatorForm() {
    var param = {
        date: $("#dateOfBirth").val(),
    };

    $.ajax({
        type: "GET",
        url: "/Home/Calculator",
        dataType: "json",
        data: param,
        success: function (data) {
            //console.log("success - submitCalculatorForm");
            if (data != null) {
                $("#months").text(data.months);
                $("#days").text(data.days);
                $("#hours").text(data.hours);
                $("#minutes").text(data.minutes);
                $("#seconds").text(data.seconds);

                $("#invalidData").hide();
                $("#result").show();

            } else {
                $("#result").hide();
                $("#invalidData").show();
            }

            $("#age").show();
        },
        failure: function (data) {
            alert(data.responseText);
            //console.log("failure");
        }, //End of AJAX failure function
        error: function (data) {
            //debugger
            alert(data.responseText);
            //console.log("error");
        } //End of AJAX error function
    });
}

function ValidateDate() {
    //console.log("ValidateDate");
    $("#age").hide();
    var dateString = $("#dateOfBirth").val();
    var regex = /(([1-9]|1\d|2\d|3[0-1])\/(0[1-9]|1[0-2])\/(000[1-9]|00[1-9]\d|0[1-9]\d\d|[1-9]\d\d\d))$/;//year - (19|20)\d\d
    if (regex.test(dateString)) {
        $("#dateError").hide();
        $("#submit").prop('disabled', false);
    } else {
        $("#dateError").show();
        $("#submit").prop('disabled', true);
    }
}