﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Age_Calculator.Models;
using Microsoft.AspNetCore.Mvc;

namespace Age_Calculator.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Calculator(string date)
        {
            if (IsValidDate(date))
            {
                DateTime birth = DateTime.Parse(date);
                DateTime current = DateTime.Now;

                Age age = new Age();
                //age.Years = current.Year - birth.Year - 1; 
                //age.Months = (current.Year - birth.Year - 1) * 12 + 12 - birth.Month + current.Month - 1; 
                //age.Days = DateTime.DaysInMonth(birth.Year, birth.Month) - birth.Day + current.Day - 1;
                //age.Hours = current.Hour; (...)
                //age.Minutes = current.Minute; (...)
                //age.Seconds = current.Second; (...)

                age.Months = (current.Year - birth.Year - 1) * 12 + 12 - birth.Month + current.Month - 1;
                int days = DateTime.DaysInMonth(birth.Year, birth.Month) - birth.Day + current.Day - 1;
                if (days >= 30) age.Months++;
                age.Days = (current.Date - birth.Date).Days;
                age.Hours = age.Days * 24;
                age.Minutes = age.Hours * 60;
                age.Seconds = (long)age.Minutes * 60;

                return Json(age);
            }
            else
            {
                return Json(null);
            }
        }

        private bool IsValidDate(string date)
        {
            if (!DateTime.TryParse(date, out DateTime tempDate)) return false;
            if ((DateTime.Parse(date).Date - DateTime.Now.Date).Days >= 0) return false;
            return true;
        }
    }
}