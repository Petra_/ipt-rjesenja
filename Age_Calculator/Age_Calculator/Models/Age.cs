﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Age_Calculator.Models
{
    public class Age
    {
        //public int Years { get; set; }
        public int Months { get; set; }
        public int Days { get; set; }

        public int Hours { get; set; }
        public int Minutes { get; set; }
        public long Seconds { get; set; }
    }
}
